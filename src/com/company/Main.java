package com.company;

public class Main {

    public static void main(String[] args) {
        // Example 1;
        Tree firstTree = new Tree();
        firstTree.insert(1);
        firstTree.insert(2);
        firstTree.insert(3);
        Tree secondTree = new Tree();
        secondTree.insert(1);
        secondTree.insert(2);
        secondTree.insert(3);
        System.out.println(isSameTree(firstTree.getRoot(), secondTree.getRoot()));
        firstTree.traverse();
        System.out.println();
        secondTree.traverse();
        System.out.println();

        //Example 2;
        Tree firstTreeExampleTwo = new Tree(1);
        firstTreeExampleTwo.insertRight(2);
        Tree secondTreeExampleTwo = new Tree(1);
        secondTreeExampleTwo.insertLeft(2);

        System.out.println(isSameTree(firstTreeExampleTwo.getRoot(), secondTreeExampleTwo.getRoot()));
        firstTreeExampleTwo.traverse();
        System.out.println();
        secondTreeExampleTwo.traverse();
        System.out.println();


        //Example 3;
        Tree firstTreeExampleThree = new Tree(1);
        firstTreeExampleThree.insertRight(2);
        firstTreeExampleThree.insertLeft(1);
        Tree secondTreeExampleThree = new Tree(1);
        secondTreeExampleThree.insertLeft(2);
        secondTreeExampleThree.insertRight(1);

        System.out.println(isSameTree(firstTreeExampleThree.getRoot(), secondTreeExampleThree.getRoot()));
        firstTreeExampleThree.traverse();
        System.out.println();
        secondTreeExampleThree.traverse();
        System.out.println();



    }

    public static boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null)
            return true;

        if (p != null && q != null)
            return (p.getData() == q.getData()
                    && isSameTree(p.getLeftChild(), q.getLeftChild())
                    && isSameTree(p.getRightChild(), q.getRightChild()));
        return false;
    }

}
