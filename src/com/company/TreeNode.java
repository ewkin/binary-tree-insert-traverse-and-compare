package com.company;

public class TreeNode {
    private int data;
    private TreeNode leftChild;
    private TreeNode rightChild;

    public void insert(int value) {
        if (value == data) {
            return;
        }

        if (value < data) {
            if (leftChild == null) {
                leftChild = new TreeNode(value);
            } else {
                leftChild.insert(value);
            }
        } else {
            if (rightChild == null) {
                rightChild = new TreeNode(value);
            } else {
                rightChild.insert(value);
            }
        }
    }

    public void insertRight(int value) {

        if (rightChild == null) {
            rightChild = new TreeNode(value);
        } else {
            rightChild.insertRight(value);
        }
    }

    public void insertLeft(int value) {

        if (leftChild == null) {
            leftChild = new TreeNode(value);
        } else {
            leftChild.insertLeft(value);
        }
    }


    public void traverse() {
        if (leftChild != null) {
            leftChild.traverse();
        }
        System.out.print(data + ", ");
        if (rightChild != null) {
            rightChild.traverse();
        }
    }

    public TreeNode(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }


    public TreeNode getLeftChild() {
        return leftChild;
    }


    public TreeNode getRightChild() {
        return rightChild;
    }

}
